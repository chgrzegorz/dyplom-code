install:
	pip install -r requirements-dev.txt

tests:
	pytest -vv

cover:
	pytest -sxvv --cov=influ/ --cov-report term-missing --skipslow test/unit && pylama
