# Change log

## [0.3.0]
### Added
- User documentation

### Changed
- Changed License to GNU GPLv2

## [0.2.0] - 2018-12-14

### Added
- Model enum
- plot_influence function
- Windows installation documentation

### Changed
- Fix influence models evaluation with seed of random function
- Few minor fixes

## [0.1.0] - 2018-12-01

### Added
- Loading graph from file
- Finding key nodes in graph (SeedFinder)
- Loading example datasets from http://konect.uni-koblenz.de
