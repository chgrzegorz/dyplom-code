import random
from unittest.mock import patch

import igraph as ig
import pytest

from influ.finder.model import linear_threshold, _apply_param, independent_cascade
from test.unit.conftest import check_konect


@pytest.mark.parametrize('graph,initial,kwarg_dict,expected', [
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(4)),
                    'treshold': [0, 0.5, 0.5, 0.5]
                },
                edges=[(0, 1), (0, 2), (0, 3)],
                edge_attrs={
                    'weight': [0.5, 0.51, 0.49]
                },
                directed=True
            ),
            [],
            {},
            set()
    ),
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(4)),
                    'treshold': [0, 0.5, 0.5, 0.5]
                },
                edges=[(0, 1), (0, 2), (0, 3)],
                edge_attrs={
                    'weight': [0.5, 0.51, 0.49]
                },
                directed=True
            ),
            [0],
            {},
            {0, 1, 2}
    ),
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(3)),
                    'treshold': [0, 0.6, 0.2]
                },
                edges=[(0, 1), (0, 2), (2, 1)],
                edge_attrs={
                    'weight': [0.3, 0.3, 0.3]
                },
                directed=True
            ),
            [0],
            {},
            {0, 1, 2}
    ),
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(3))
                },
                edges=[(0, 1), (0, 2), (2, 1)],
                edge_attrs={
                    'weight': 0.3
                },
                directed=True
            ),
            [0],
            {
                'threshold': [0, 0.6, 0.2],
                'depth': 1
            },
            {0, 2}
    ),
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(4))
                },
                edges=[(2, 1), (3, 2), (3, 1)],
                edge_attrs={
                    'weight': 0.3
                },
                directed=True
            ),
            [0],
            {
                'threshold': [0, 0.6, 0.2],
                'depth': 1
            },
            {0}
    ),
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(11))
                },
                edges=[(i + 1, i) for i in range(10)],
                edge_attrs={'weight': 1}, directed=True),
            [0],
            {
                'threshold': 0
            },
            {0}
    )

])
def test_linear_threshold(graph, initial, kwarg_dict, expected):
    assert linear_threshold(graph, sorted(initial), **kwarg_dict) == expected
    assert linear_threshold(graph, sorted(initial, reverse=True), **kwarg_dict) == expected


@pytest.mark.skipif(check_konect(), reason="Cannot connect to Konect")
@pytest.mark.parametrize('iids', [
    [1, 2, 3, 4, 5],
    [100],
    [7, 13, 15, 16, 23, 147, 20]
])
def test_LT_deterministic(manufacturing_email, iids):
    a = linear_threshold(manufacturing_email, initial_ids=iids, depth=1, random_seed=10)
    b = linear_threshold(manufacturing_email, initial_ids=iids, depth=1, random_seed=10)
    random.shuffle(iids)
    c = linear_threshold(manufacturing_email, initial_ids=iids, depth=1, random_seed=10)
    assert a == b
    assert a == c


@pytest.mark.skipif(check_konect(), reason="Cannot connect to Konect")
@pytest.mark.parametrize('iids', [
    [1, 2, 3, 4, 5],
    [100],
    [7, 13, 15, 16, 23, 147, 20]
])
def test_IC_deterministic(manufacturing_email, iids):
    a = independent_cascade(manufacturing_email, initial_ids=iids, depth=1, random_seed=10)
    b = independent_cascade(manufacturing_email, initial_ids=iids, depth=1, random_seed=10)
    assert a == b


@pytest.mark.parametrize('graph,initial,kwarg_dict,random_values,expected', [
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(4)),
                    'treshold': 0.5
                },
                edges=[(0, 1), (0, 2), (0, 3)],
                directed=True
            ),
            [],
            {},
            [0.51, 0.5, 0.49],
            set()
    ),
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(4)),
                    'treshold': 0.5
                },
                edges=[(0, 1), (0, 2), (0, 3)],
                directed=True
            ),
            [0],
            {},
            [0.51, 0.5, 0.49],
            {0, 1, 2}
    ),
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(4)),
                    'treshold': [0.5, 0, 0.8]
                },
                edges=[(0, 1), (0, 2), (2, 1)]
            ),
            [0],
            {'depth': 1},
            [0.49, 0.51, 0.85],
            {0, 2}
    ),
    (
            ig.Graph(
                vertex_attrs={
                    'id': list(range(4))
                },
                edges=[(0, 1), (0, 2), (2, 1)]
            ),
            [0],
            {'threshold': [0.5, 0, 0.8]},
            [0.49, 0.51, 0.85],
            {0, 1, 2}
    )
])
def test_independent_cascade(graph, initial, kwarg_dict, random_values, expected):
    rwi = iter(random_values)
    with patch('influ.finder.model.random.random', new=lambda: next(rwi)):
        assert independent_cascade(graph, sorted(initial), **kwarg_dict) == expected

    rwi = iter(random_values)
    with patch('influ.finder.model.random.random', new=lambda: next(rwi)):
        assert independent_cascade(graph, sorted(initial, reverse=True), **kwarg_dict) == expected


@pytest.mark.parametrize('threshold,expected', [
    (0.3, 0.3),
    (lambda: 0.3, 0.3)
])
def test_apply_param(threshold, expected, cheq):
    graph = ig.Graph(n=5)
    param_name = 'some_param'
    _apply_param(graph, param_name, value=threshold)

    cheq(graph, ig.Graph(n=5, vertex_attrs={param_name: expected}))


@patch('influ.finder.model.random.random', return_value=0.333)
def test_apply_param_random(cheq):
    graph = ig.Graph(n=5)
    param_name = 'some_param'
    _apply_param(graph, param_name)

    cheq(graph, ig.Graph(n=5, vertex_attrs={param_name: 0.333}))
