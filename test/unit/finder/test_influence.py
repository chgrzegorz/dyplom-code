import igraph as ig
import pytest

from influ.finder.influence import SeedFinder
from influ.reader import KonectReader
from test.unit.conftest import load_or_None, check_konect


@pytest.mark.parametrize('graph,expected', [
    (ig.Graph(edges=[(0, 1), (0, 1), (1, 0)], directed=True), [1, 0]),
    (ig.Graph(edges=[(0, 2), (1, 0), (1, 0), (2, 0)], directed=True), [1, 0, 2]),
    (ig.Graph(edges=[(2, 0), (1, 0), (1, 0), (0, 1), (0, 2), (1, 2)], directed=True), [0, 2, 1]),
    (ig.Graph(edges=[(1, 0), (1, 0), (2, 0)], directed=True), [0, 1, 2]),

])
def test_indegree(graph, expected):
    assert set(SeedFinder(graph, len(graph.vs), 'number').by_indegree()) == set(expected)  # order may be valuable


@pytest.mark.parametrize('graph,expected', [
    (ig.Graph(edges=[(0, 1), (0, 1), (1, 0)], directed=True), [0, 1]),
    (ig.Graph(edges=[(0, 2), (1, 0), (1, 0), (2, 0)], directed=True), [1, 0, 2]),
    (ig.Graph(edges=[(1, 0), (1, 0), (2, 0)], directed=True), [1, 2, 0]),

])
def test_outdegree(graph, expected):
    assert set(SeedFinder(graph, len(graph.vs), 'number').by_outdegree()) == set(expected)  # order may be valuable


@pytest.mark.parametrize('graph,expected', [
    (ig.Graph(edges=[(0, 1), (0, 1), (1, 0)]), [0, 1]),
    (ig.Graph(edges=[(0, 2), (1, 0), (1, 0), (2, 0)]), [0, 1, 2]),
    (ig.Graph(edges=[(1, 2), (1, 2), (2, 0), (0, 2), (2, 1)]), [2, 1, 0]),
])
def test_degree(graph, expected):
    assert SeedFinder(graph, len(graph.vs), 'number').by_degree() == expected


@pytest.mark.parametrize('graph,expected', [
    (ig.Graph(edges=[(0, 1), (0, 1), (1, 0)]), [0, 1]),
    (ig.Graph(edges=[(0, 2), (1, 0), (1, 0), (2, 0)]), [0, 1, 2]),
    (ig.Graph(edges=[(1, 2), (1, 2), (2, 0), (0, 2), (2, 1)]), [2, 0, 1]),
    (ig.Graph(edges=[(0, 1), (1, 2), (2, 3), (3, 4)]), [2, 1, 3, 0, 4]),

])
def test_betweenness(graph, expected):
    assert SeedFinder(graph, len(graph.vs), 'number').by_betweenness() == expected


@pytest.mark.parametrize('graph,expected', [
    (ig.Graph(edges=[(0, 4), (1, 2), (1, 3), (2, 3), (3, 4)]), [1, 2, 3, 4, 0]),
    (ig.Graph(edges=[(0, 4), (1, 2), (1, 3), (2, 3), (3, 4)], directed=True), []),
    (ig.Graph(edges=[(0, 4), (4, 0), (1, 2), (2, 1), (1, 3), (3, 1), (3, 2), (2, 3), (3, 4), (4, 3)], directed=True),
     [1, 2, 3, 4, 0]),
])
def test_clustering_coefficient(graph, expected):
    assert SeedFinder(graph, len(graph.vs), 'number').by_clustering_coefficient() == expected


@pytest.mark.parametrize('graph,model,numberof,unit,expected', [
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)],
                     edge_attrs={'weight': 1}, directed=True),
            'LT', 1, 'number',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)],
                     edge_attrs={'weight': 1}, directed=True),
            'LT', 10, 'percent',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)], directed=True),
            'IC', 1, 'number',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)], directed=True),
            'IC', 10, 'percent',
            [10]
    ),
])
def test_greedy_obvious(graph, model, numberof, unit, expected):
    assert SeedFinder(graph, numberof, unit).greedy(model=model, threshold=0) == expected


@pytest.mark.parametrize('graph,model,numberof,unit,expected', [
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)],
                     edge_attrs={'weight': 1}, directed=True),
            'LT', 1, 'number',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)],
                     edge_attrs={'weight': 1}, directed=True),
            'LT', 10, 'percent',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)], directed=True),
            'IC', 1, 'number',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)], directed=True),
            'IC', 10, 'percent',
            [10]
    ),
])
def test_brute_force_obvious(graph, model, numberof, unit, expected):
    assert SeedFinder(graph, numberof, unit).brute_force(model=model, threshold=0) == expected


@pytest.mark.slow
@pytest.mark.parametrize('graph,numberof', [
    (
            ig.Graph.Barabasi(n=100, m=10),
            100
    ),
    (
            ig.Graph.GRG(n=100, radius=7),
            2
    ),
])
def test_greedy_deterministic(graph, numberof):
    run1 = SeedFinder(graph, numberof, 'number', random_seed=12).greedy(model='IC')
    run2 = SeedFinder(graph, numberof, 'number', random_seed=12).greedy(model='IC')
    assert run1 == run2


@pytest.mark.slow
@pytest.mark.parametrize('graph,numberof', [
    (
            ig.Graph.Barabasi(n=100, m=10),
            100
    ),
    (
            ig.Graph.GRG(n=100, radius=7),
            2
    ),
])
def test_brute_force_deterministic(graph, numberof):
    run1 = SeedFinder(graph, numberof, 'number', random_seed=12).brute_force(model='IC')
    run2 = SeedFinder(graph, numberof, 'number', random_seed=12).brute_force(model='IC')
    assert run1 == run2


@pytest.mark.slow
@pytest.mark.parametrize('graph,numberof', [
    (
            ig.Graph.Barabasi(n=100, m=10),
            100
    ),
    (
            ig.Graph.GRG(n=100, radius=7),
            2
    ),
])
def test_CELFpp_deterministic(graph, numberof):
    run1 = SeedFinder(graph, numberof, 'number', random_seed=12).CELFpp(model='IC')
    run2 = SeedFinder(graph, numberof, 'number', random_seed=12).CELFpp(model='IC')
    assert run1 == run2


@pytest.mark.parametrize('graph,model,numberof,unit,expected', [
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)],
                     edge_attrs={'weight': 1}, directed=True),
            'LT', 1, 'number',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)],
                     edge_attrs={'weight': 1}, directed=True),
            'LT', 10, 'percent',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)], directed=True),
            'IC', 1, 'number',
            [10]
    ),
    (
            ig.Graph(edges=[(i + 1, i) for i in range(10)], directed=True),
            'IC', 10, 'percent',
            [10]
    ),
])
def test_CELFpp_obvious(graph, model, numberof, unit, expected):
    assert SeedFinder(graph, numberof, unit).CELFpp(model=model, threshold=0) == expected


@pytest.mark.parametrize('items,n,expected', [
    (
            [[1, 3, 5], [], [2, 4]],
            5,
            [1, 2, 3, 4, 5]
    ),
    (
            [[1, 3, 5], [], [2, 4]],
            3,
            [1, 2, 3]
    ),
    (
            [[1, 3], [], [2, 4], []],
            5,
            [1, 2, 3, 4]
    ),
    (
            [[1, 3], [2, 4, 6, 8, 10, 12]],
            7,
            [1, 2, 3, 4, 6, 8, 10]
    )
])
def test_merge_results(items, n, expected):
    assert SeedFinder(ig.Graph(n=n), number=n, unit='number')._merge_results(items) == expected


kr = KonectReader()
manufacturing_emails = load_or_None('manufacturing_emails')
train_bombing = load_or_None('train_bombing')
david_copperfield = load_or_None('david_copperfield')


@pytest.mark.skipif(check_konect(), reason="Cannot connect to Konect")
@pytest.mark.slow
@pytest.mark.parametrize('graph,numberof,model', [
    (manufacturing_emails, 1, 'IC'),
    (manufacturing_emails, 1, 'LT'),
    # (manufacturing_emails, 3, 'IC'),
    # (manufacturing_emails, 3, 'LT'),
    # (train_bombing, 1, 'IC'),
    # (train_bombing, 1, 'LT'),
    (train_bombing, 3, 'IC'),
    # (train_bombing, 3, 'LT'),
    (david_copperfield, 1, 'IC'),
    (david_copperfield, 1, 'LT'),
    (david_copperfield, 3, 'IC'),
    # (david_copperfield, 3, 'LT'),
])
def test_greedy_celfpp(graph, numberof, model):
    spread = SeedFinder(graph, numberof, 'number', random_seed=12)._define_spread(model, None, 1)
    greedy_1 = SeedFinder(graph, numberof, 'number', random_seed=12).greedy(model=model, depth=1)
    greedy_2 = SeedFinder(graph, numberof, 'number', random_seed=12).greedy(model=model, depth=1)
    celfpp_1 = SeedFinder(graph, numberof, 'number', random_seed=12).CELFpp(model=model, depth=1)
    celfpp_2 = SeedFinder(graph, numberof, 'number', random_seed=12).CELFpp(model=model, depth=1)

    assert spread(greedy_1) == spread(greedy_2)
    assert set(greedy_1) == set(greedy_2)

    assert spread(celfpp_1) == spread(celfpp_2)
    assert set(celfpp_1) == set(celfpp_2)

    assert spread(celfpp_1) == spread(greedy_1)
    assert set(greedy_1) == set(celfpp_1)
