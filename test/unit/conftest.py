import os
from warnings import warn

import igraph as ig
import pytest

from influ.reader import KonectReader
from influ.reader.konect import DownloadFailure


def pytest_addoption(parser):
    parser.addoption("--skipslow", action="store_true", default=False, help="skip slow tests")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--skipslow"):
        skip_slow = pytest.mark.skip(reason="because --skipslow")
        for item in items:
            if "slow" in item.keywords:
                item.add_marker(skip_slow)


@pytest.fixture
def example_filepath():
    return os.path.abspath("test/example_data/manufacturing_emails.csv")


@pytest.fixture
def small_graph():
    return ig.Graph(
        edges=list(zip([0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 4, 4, 5, 6],
                       [1, 1, 2, 0, 2, 2, 1, 2, 2, 5, 6, 5, 6, 6, 6]
                       )),
        vertex_attrs={'id': list(range(7))},
        edge_attrs={'id': range(14)},
        directed=True
    )


@pytest.fixture
def simple_small_graph():
    return ig.Graph(
        edges=list(zip([1, 0, 2, 0, 1, 3, 4, 4, 5],
                       [0, 1, 1, 2, 2, 2, 5, 6, 6]
                       )),
        edge_attrs={
            'id': [[3], [0, 1], [6], [2], [4], [7, 8], [9, 11], [10, 12], [13]],
            'weight': [1 / 1, 2 / 3, 1 / 3, 1 / 4, 1 / 4, 2 / 4, 2 / 2, 2 / 3, 1 / 3]

        },
        vertex_attrs={'id': list(range(7))},
        directed=True
    )


@pytest.fixture(scope='session')
def cheq():
    """Returns a function that check if two graphs are equal"""

    def repre(e):
        return e.index, (e.source, e.target), e.attributes()

    def reprv(v):
        return v.index, v.attributes()

    def assert_equal(g1: ig.Graph, g2: ig.Graph):
        assert g1.is_directed() == g2.is_directed()
        assert g1.attributes() == g2.attributes()
        assert [reprv(v) for v in g1.vs] == [reprv(v) for v in g2.vs]
        assert [repre(e) for e in g1.es] == [repre(e) for e in g2.es]

    return assert_equal


def check_konect():
    kr = KonectReader()
    try:
        kr.load('manufacturing_emails')
    except DownloadFailure:
        warn("Konect is not available", RuntimeWarning)
        return True
    return False


def load_or_None(name):
    kr = KonectReader()
    try:
        return kr.load(name)
    except DownloadFailure:
        warn("Konect is not available", RuntimeWarning)
        return None


@pytest.fixture(scope='session')
def manufacturing_email():
    return load_or_None('manufacturing_emails')


@pytest.fixture(scope='session')
def train_bombing():
    return load_or_None('train_bombing')


@pytest.fixture(scope='session')
def david_copperfield():
    return load_or_None('david_copperfield')
