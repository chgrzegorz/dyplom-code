from tempfile import NamedTemporaryFile

import igraph as ig
import pandas as pd

from influ.reader.merge import merge_edges
from influ.reader.direct import _df_to_graph, read_graph


def test_df_to_graph(example_filepath):
    data = pd.read_csv(example_filepath)
    result = _df_to_graph(data)

    assert result.is_directed()
    assert result.vcount() == 167
    assert result.ecount() == 82927


def test_read_graph_events(cheq):
    graph_txt = [
        'from,to,timestamp,weight\n',
        '1,2,15420587643,0.5\n',
        '2,1,15420587644,0.64\n',
        '3,2,15420587645,0.2\n',
        '1,3,15420587643,1\n'
    ]
    expected = ig.Graph(edges=[(0, 1), (1, 0), (2, 1), (0, 2)],
                        edge_attrs={
                            'timestamp': [15420587643, 15420587644, 15420587645, 15420587643],
                            'weight': [0.5, 0.64, 0.2, 1.0],
                        },
                        vertex_attrs={'id': list(range(3))},
                        directed=True)

    expected = merge_edges(expected)

    with NamedTemporaryFile(mode='w') as file:
        for row in graph_txt:
            file.write(row)
        file.seek(0)
        result = read_graph(file.name, 'events', sep=',')

        cheq(result, expected)
