import igraph as ig
import pytest
from pandas import read_csv

from influ.reader.merge import merge_edges, EdgeSum, sum_edges
from influ.reader.direct import _df_to_graph


def test_merge_edges(small_graph, simple_small_graph, cheq):
    result = merge_edges(small_graph)

    cheq(result, simple_small_graph)


@pytest.mark.slow
def test_merge_manufacturing(example_filepath):
    result = merge_edges(_df_to_graph(read_csv(example_filepath)))
    assert result.is_directed()
    assert result.vcount() == 167
    assert result.ecount() == 5783


@pytest.mark.parametrize('graph,target,sums', [
    (ig.Graph(), None, ()),
    (
            ig.Graph(edges=[(0, 1), (0, 1), (1, 0), (1, 2), (2, 1)],
                     directed=True),
            1,
            (EdgeSum(0, 1, 2, {'weight': 2 / 3}),)
    ),
    (
            ig.Graph(edges=[(0, 1), (0, 1), (1, 0), (1, 2), (2, 1)],
                     edge_attrs={'i': [0, 1, 2, 3, 4], 'j': ['a', 'b', 'c', 'd', 'e']},
                     directed=True),
            1,
            (EdgeSum(0, 1, 2, {'i': [0, 1], 'j': ['a', 'b'], 'weight': 2 / 3}),)
    )

])
def test_sum_edges(graph, target, sums):
    result_sums = sum_edges(graph, target)
    for a, b in zip(result_sums, sums):
        assert a == b
