FROM python:3.7-stretch
RUN apt-get update
RUN apt-get install -y \
    make \
    build-essential python-dev libxml2 libxml2-dev zlib1g-dev

RUN mkdir /code
WORKDIR /code
COPY requirements.txt .

RUN pip install -r requirements.txt
