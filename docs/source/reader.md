# Reader
```python
from influ import reader
```
## read_graph
Read graph from file if it is saved in "events" format. Otherwise igraph read method is used.
*Parameters*:
* **filepath** [required] - path to file with graph
* **file_format** [[optional, default: None] - optional (but recommended) format name; 
eg: `events` for InFlu native file format
`ncol` for NCOL format
* **kwargs** [[optional] - additional keyword arguments specific to source file type: `directed`, `sep` etc.

## konect_to_csv
Converts konect dataset to format readable by influ
*Parameters*:
* **source_path** [required] - path to the "out" file downloaded from konect
* **dest_path** [required] - path to destination where to save a file
* **edge_attrs** [required] - optional edge attributes that are known to exists in out file
if this parameter is missing or there are more attributes then unnamed attributes will be named as attr{index}

## KonectReader
Interface to download configured datasets from [Konect](http://konect.uni-koblenz.de/)
```python
from influ import reader

kr = reader.KonectReader()
print(kr.list) # list available datasets
graph = kr.load(kr.list[0])
```
### KonectReader . list
List of names of available datasets
### KonectReader . add_config
Add datasets specified by user in configuration file. Detailed description of configuration file can be found in "First steps - Your own config file" section.
*Parameters*:
* **path** [required] - path to config file

### KonectReader . load
Loads dataset from Konect, and return loaded graph
*Parameters*:
* **dataset_name** [required] - name of the dataset to download; it have to be one of names from specified in `list` attribute
