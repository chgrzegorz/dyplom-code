Welcome to InFlu!
=================================
.. image:: https://gitlab.com/chgrzegorz/dyplom-code/badges/develop/pipeline.svg
   :target: https://gitlab.com/chgrzegorz/dyplom-code/commits/develop
   :alt: Build Status

InFlu is an application for social network analysis for finding key nodes in the social influence process as the main feature.
It gives you possibility to choose nodes based on the typical SNA metrics and using two models of social influence:
Linear Threshold and Independent Cascade.
The are three methods that allow you to use influence models to find the key nodes:
brute-force, greedy and improved greedy known as CELF++ proposed in `this paper <https://www.cs.ubc.ca/~goyal/research/celf++.pdf>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   reader
   finder
