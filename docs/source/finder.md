# Finder
```python
from influ import finder
```

## SeedFinder
This is basic object for finding key nodes in your network. Works for both directed and undirected graphs.
Graphs needs to have special structure and because of that only graphs loaded with `reader` module are recommended.

*Initialization parameters*
* **graph** [required] – graph that will be analysed
* **number** [optional, default: 5] - value of number or percentage of seeds to choose
* **unit** [optional, default: 'number'] - either `percent` or `number`;
* **random_seed** [optional, default: None] – value used as seed for random function to ensure repetitive results; 

### SeedFinder . configure
*Parameters*:
* **number** [optional, default: None] – value of number or percentage of seeds to choose; have to be configured together with `unit` parameter
* **unit** [optional, default: None] – either `percent` or `number`; have to be configured together wit `number` parameter
* **random_seed** [optional, default: None] – value used as seed for random function to ensure repetitive results.
It's used at the beginning of every model evaluation. If `random_seed` is equal to `None` (default) then no random seed will be used

### SeedFinder . by_indegree
Return list of n first vertices indices sorted by their indegree.
Takes no parameters.

### SeedFinder . by_outdegree
Return list of n first vertices indices sorted by their outdegree.
Takes no parameters.

### SeedFinder . by_degree
Return list of n first vertices indices sorted by their degree.
Takes no parameters.

### SeedFinder . by_betweenness
Return list of n first vertices indices sorted by their betweenness.
Takes no parameters.

### SeedFinder . by_clustering_coefficient
Return list of n first vertices indices sorted by their clustering coefficient (transitivity).
        IMPORTANT: in directed graph only mutual edges will be considered
Takes no parameters.

### SeedFinder . greedy
Search for vertices indices that are the best seeds using greedy approach.

*Parameters*:
* **model** [optional, default: Model.LinearThreshold] - model of social influence.
    Currently only Linear Treshold (LT) and Independent Cascade (IC) are available
* **threshold** [optional, default: None] - defines value of threshold in influence model.
    In Linear Threshold model it defines threshold of sum of influence
    that have to applied to node to activate it.
    In Independent Cascade model it's probability that activated node activates another node.
* **depth** [optional, default: None] - how many iterations will be in spreading simulations
:return: list of ids of nodes considered as the best seeds

### SeedFinder . brute_force
Search for vertices indices that are the best seeds using brute force approach.

*Parameters*:
* **model** [optional, default: Model.LinearThreshold] - model of social influence.
    Currently only Linear Treshold (LT) and Independent Cascade (IC) are available
* **threshold** [optional, default: None] - defines value of threshold in influence model.
    In Linear Threshold model it defines threshold of sum of influence
    that have to applied to node to activate it.
    In Independent Cascade model it's probability that activated node activates another node.
* **depth** [optional, default: None] - how many iterations will be in spreading simulations
:return: list of ids of nodes considered as the best seeds

### SeedFinder . CELFpp
Search for vertices indices that are the best seeds using CELF++ approach.

*Parameters*:
* **model** [optional, default: Model.LinearThreshold] - model of social influence.
    Currently only Linear Treshold (LT) and Independent Cascade (IC) are available
* **threshold** [optional, default: None] - defines value of threshold in influence model.
    In Linear Threshold model it defines threshold of sum of influence
    that have to applied to node to activate it.
    In Independent Cascade model it's probability that activated node activates another node.
* **depth** [optional, default: None] - how many iterations will be in spreading simulations
:return: list of ids of nodes considered as the best seeds

### SeedFinder . plot_influence
Run influence simulation for given set of seed and plot result graph.

*Parameters*:
* **seeds** [required] - list of seed ids for influence spreading simulation
* **model** [optional, default: Model.LinearThreshold] - model of social influence.
    Currently only Linear Treshold (LT) and Independent Cascade (IC) are available
* **threshold** [optional, default: None] - defines value of threshold in influence model.
    In Linear Threshold model it defines threshold of sum of influence
    that have to applied to node to activate it.
    In Independent Cascade model it's probability that activated node activates another node.
* **depth** [optional, default: None] - how many iterations will be in spreading simulations
:return: list of ids of nodes considered as the best seeds


## Model
Social influence model enum

### Model . LinearThreshold
Enum value.
Represents Linear Threshold Model

### Model . IndependentCascade
Enum value.
Represents Independent Cascade Model
